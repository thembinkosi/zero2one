<?php


namespace App\Observers;


use App\User;
use Mail;

class EmailObserver
{
    public function updating (User $user)
    {

        if ($user->isDirty('email')) {
            // email has changed
            $new_email = $user->email;
            $old_email = $user->getOriginal('email');

            Mail::send(
                'mail.notification',
                compact('user','old_email', 'new_email'),
                function($message) use ($user, $old_email) {
                    $message->to($old_email, $user->name)
                        ->subject('Mail Change Notification');
                });
        }
    }
}
