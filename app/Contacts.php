<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contacts extends Model
{
    protected $fillable = [
        'name', 'number', 'contact_profile_picture', 'user_id'
    ];
}
