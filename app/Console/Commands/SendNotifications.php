<?php

namespace App\Console\Commands;

use App\Notifications\UserNotification;
use App\User;
use Illuminate\Console\Command;
use Notification;

class SendNotifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:notifications';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send database Notifications to all users in the system';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::all();
        foreach ($users as $user){

            Notification::send($user, new UserNotification());
        }
    }
}
