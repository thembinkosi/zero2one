<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = User::find(Auth::id());
        $rules =array();
        if (strcmp($user->user_profile_picture, $this->file) !== 0){
            $rules = [
                'email'                 =>      'required|regex:/^.+@.+$/i',
                'name'                  =>      'required',
                'file'                  =>      'required|image|mimes:jpeg,png,jpg|max:200000'
            ];
        }
        else{
            $rules = [
                'email'                 =>      'required|regex:/^.+@.+$/i',
                'name'                  =>      'required',
            ];
        }

        return $rules;
    }
}
