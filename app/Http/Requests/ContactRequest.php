<?php

namespace App\Http\Requests;

use App\Contacts;
use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        if( strcmp(Contacts::find($this->id)->contact_profile_picture, $this->file) !== 0 ){
            $rules = [
                'number'                =>  'required|digits_between:6,10',
                'name'                  =>  'required',
                'file'                  =>      'required|image|mimes:jpeg,png,jpg|max:200000'
            ];
        }else{
            $rules = [
                'number'                =>  'required|digits_between:6,10',
                'name'                  =>  'required',
            ];
        }

       return $rules;
    }
}
