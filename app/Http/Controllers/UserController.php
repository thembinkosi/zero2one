<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Get Auth user Data
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUserData(){
        return view('user')->with(['user' => auth()->user()]);
    }

    /**
     * This function updates Auth user details
     * @param Request $request
     * @return string
     */
    public function updateUserDetails(UserRequest $request)
    {
        $user_id = auth()->user()->id;

        User::find($user_id)->update([
            'email'     => $request['email'],
            'name'     => $request['name']
        ]);

        $user = User::find($user_id);

        if (strcmp($user->user_profile_picture, $request->file) !== 0){
            $imageController = new ImageUploadController();
            $results = $imageController->uploadImage($request, $user_id);
        }

        return 'success';
    }
}
