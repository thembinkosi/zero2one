<?php

namespace App\Http\Controllers;

use App\Contacts;
use App\Http\Controllers\ImageUploadController;
use App\Http\Requests\ContactRequest;
use App\Http\Requests\UserRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class ContactsController extends Controller
{
    /**
     * Returns all Contacts related to Auth user
     * @return false|string
     */
    public function getUserContacts()
    {
        return json_encode(auth()->user()->contacts);
    }

    /**
     * This function creates a new Contact
     * @param ContactRequest $request
     * @return string
     */
    public function create(ContactRequest $request)
    {

        if (strcmp(Contacts::find($request->id)->contact_profile_picture, $request->file) !== 0){
            $imageController = new ImageUploadController();
            $results = $imageController->uploadImage($request);
        }else{
            Contacts::find($request->id)->update([
                'name'                      =>  $request->name,
                'number'                    =>  $request->number,
            ]);
        }

        return 'success';
    }

    public function delete($id)
    {
        Contacts::find($id)->delete();
        return 'success';
    }

}
