<?php

namespace App\Http\Controllers;

use App\Contacts;
use App\Http\Requests\ImageValidatorRequest;
use App\PersonBiography;
use App\User;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class ImageUploadController extends Controller
{

    /**
     * Validates file extension type
     *
     * @param $file_ext, $file_size, $file_tmp, $file_error
     * @return bool
     */
    public function validateImgFile($file_ext, $file_size, $file_tmp, $file_error) {

        $hasDimensions = getimagesize($file_tmp);
        $validExtensions = array("jpeg", "jpg", "png");
        $isValidExtension = in_array($file_ext, $validExtensions);

        if($file_error && $file_size <= 200000 && $hasDimensions && $isValidExtension) {

            return true;

        } else {

            return false;
        }
    }

    /**
     * Create image file name, uploads to storage and store in DB
     *
     * @param $file_ext, $file_size, $file_tmp, $file_error
     * @return string
     */
    public function uploadImage($request , $cid = null)
    {
        $image_file = Input::file('file');
        $file_name = strtolower(pathinfo($image_file, PATHINFO_BASENAME)); //Get file basename
        $file_ext = strtolower($image_file->getClientOriginalExtension()); //Get file extension
        $file_size = $request->file('file')->getSize();
        $file_tmp = $request->file('file')->getPathName(); //Tmp filename from browser temp memory
        $file_error = $request->file('file')->isValid();;
        $fileIsValid = $this->validateImgFile($file_ext, $file_size, $file_tmp, $file_error);
//        $ImgOld = Contacts::select('contact_profile_picture')->where('id');
        $ImgOld = null;

        if($fileIsValid) {

            $file_name = filter_var($file_name, FILTER_SANITIZE_STRING);
            $datetime = date("ymd-His");

            if ($file_ext == 'jpeg') {

                $file_name = substr_replace(preg_replace("/[-_ ()]/", " ", $file_name), $cid."-".$datetime, 0, -5);

            } else {

                $file_name = substr_replace(preg_replace("/[-_ ()]/", " ", $file_name), $cid."-".$datetime, 0, -4);
            }

            $file_name = $file_name.'.'.$file_ext;

            if($file_ext === 'jpeg' || $file_ext === 'jpg') {

                $image = Image::make($image_file)->fit(200, 200)->stream();
                Storage::disk('local')->put($file_name, $image->__toString());

            } else if ($file_ext === 'png') {

                //Create png, resize and store in server location
                $image = Image::make($image_file)->fit(200, 200)->stream();
                Storage::disk('local')->put($file_name, $image->__toString());

            }
            //Upload file path to database
            $authUser_id = auth()->user()->id;

            if ( $authUser_id == $cid){

                User::where('id', $cid)->update([
                    'user_profile_picture'   => $file_name
                ]);

            }
            else{
                if (isset($request->id)){
                    Contacts::find($request->id)->update([
                        'name'                      =>  $request->name,
                        'number'                    =>  $request->number,
                        'contact_profile_picture'   => $file_name
                    ]);

                }else{
                    Contacts::create([
                        'name'                      =>  $request->name,
                        'number'                    =>  $request->number,
                        'user_id'                   =>  $authUser_id,
                        'contact_profile_picture'   => $file_name
                    ]);
                }

            }


            //Action if success : delete old file
            if(Storage::disk('local')->exists($ImgOld))
            {
                Storage::disk('local')->delete($ImgOld);
            }

            return 'success';

        } else {
            //Action if file invalid
            return 'invalid';

        }
    }

}
