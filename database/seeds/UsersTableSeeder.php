<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->insert([
            'name' => 'John Doe',
            'email' => 'john@email.com18',
            'user_profile_picture' => 'default.png',
            'created_at' => new \DateTime(),
            'password' => bcrypt('password'),
        ]);
    }
}
