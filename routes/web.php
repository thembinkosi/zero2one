<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');

Route::post('/contact_save', 'ContactsController@create')->name('create_contact')->middleware('auth');
//Route::put('/contact_save', 'ContactsController@create')->name('create_contact')->middleware('auth');
Route::get('/get_contacts', 'ContactsController@getUserContacts')->middleware('auth');
Route::get('/user', 'UserController@getUserData')->middleware('auth');
Route::put('/update_user', 'UserController@updateUserDetails')->middleware('auth');
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout')->middleware('auth');
Route::delete('/contact_delete/{id}', 'ContactsController@delete')->middleware('auth');

Route::get('/contact', function () {
    return view('contacts');
})->middleware('auth');

