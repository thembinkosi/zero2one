@extends('layouts.admin')
@section('content')

    <div id="app">
        <user @if(isset($user)) :user="{{ $user }}" @endif></user>
    </div>
@endsection
